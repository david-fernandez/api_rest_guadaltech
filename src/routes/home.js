const express = require('express');
const homeRouter = express.Router();

homeRouter.get('/', (req, res) => {
    res.send(`
        <div style="text-align: center;margin-top:100px;">
            <h1 style="font-size: 40px;">Bienvenidos a la API REST de guadaltech</h1>
            <div style="padding: 20px; width: 800px;height: 400px; margin:auto; margin-top: 60px; text-align: left; font-size: 24px;">
                <p>* <span style="font-weight: bold; text-decoration: underline; margin-right: 20px;">/</span> (Ruta principal del proyecto, ahora nos encontramos aqui)</p>
                <p>* <span style="font-weight: bold; text-decoration: underline; margin-right: 20px;">/persona</span> (Get de personas)</p>
                <p>* <span style="font-weight: bold; text-decoration: underline; margin-right: 20px;">/persona/:id</span> (Get de la persona cuyo id pasemos)</p>
                <p>* <span style="font-weight: bold; text-decoration: underline; margin-right: 20px;">/persona/nueva</span> (POST de una persona (username: String, email: String))</p>
                <p>* <span style="font-weight: bold; text-decoration: underline; margin-right: 20px;">/persona/modificar/:id</span> (PUT con el id de la persona que queramos modificar)</p>
                <p>* <span style="font-weight: bold; text-decoration: underline; margin-right: 20px;">/persona/borrar/:id</span> (Borramos la persona cuyo id pasamos)</p>
            </div>
        </div>
    `);
})

module.exports = homeRouter;