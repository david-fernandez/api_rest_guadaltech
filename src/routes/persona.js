const express = require('express');
const Persona = require('../models/Persona');

const personaRouter = express.Router();

personaRouter.delete('/borrar/:id', (req, res)=>{
    const id = req.params.id;
    Persona.findByIdAndDelete(id)
        .then(()=>{
            res.status(200).send({mensaje: "La persona con " + "id: " + id + " se eliminó correctamente"});
        })
        .catch((error)=>{
            res.status(500).send(error);
        })
});

personaRouter.put('/modificar/:id', (req, res)=>{
    const id = req.params.id;

    const username = req.body.username;
    const email = req.body.email;

    Persona.findByIdAndUpdate(id, {
        username: username,
        email: email
    })
        .then(()=>{
            return Persona.findById(id, {__v: 0, updatedAt: 0, createdAt: 0});
        })
        .then((personaModificada)=>{
            res.status(200).send(personaModificada);
        })
        .catch((error)=>{
            res.status(500).send(error);
        })

});

personaRouter.post('/nueva',(req, res) =>{
    const username = req.body.username;
    const email = req.body.email;

    const persona = new Persona();

    persona.username = username;
    persona.email = email;

    persona.save()
        .then((newPersona)=> {
            res.json(newPersona);
        })
        .catch((error)=> {
            res.status(500).send(error);
        })
});

personaRouter.get('/:id', (req, res)=>{
    const id = req.params.id;
    Persona.findById(id, {__v: 0, updatedAt: 0, createdAt: 0})
        .then((persona)=>{
            res.status(200).send(persona);
        })
        .catch((error)=>{
            res.status(500).send(error);
        })
});

personaRouter.get('/', (req, res)=>{
    Persona.find({}, {__v: 0, createdAt: 0, updatedAt: 0})
        .then((personas)=>{
            res.status(200).send(personas);
        })
        .catch((error)=>{
            res.status(500).send(error);
        })
});

module.exports = personaRouter;