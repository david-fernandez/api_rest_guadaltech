const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const personaSchema = new Schema(
    {
        username: { type: String, required: true },
        email: { type: String, required: true }
    },
    {
        timestamps: true,
    }
);

const Persona = mongoose.model('Persona', personaSchema);
module.exports = Persona;