const express = require('express');
require('dotenv').config();
const personaRoutes = require('./routes/persona');
const homeRoutes = require('./routes/home');
require('./db');

const PORT = process.env.PORT || 4000;
const server = express();

server.use(express.json());
server.use(express.urlencoded({extended: false}));

server.use('/persona', personaRoutes);
server.use('/', homeRoutes);

server.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`);
});