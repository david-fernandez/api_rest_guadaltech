### npm install

instalara todos los modulos del package.json, para su correcto funcionamiento.

# https://docs.mongodb.com/guides/server/install/

guia para instalar mongoDB en los diversos sistemas operativos.

# https://brew.sh/index_es

En mi caso uso brew como instalador de paquetes para MacOS.

# brew services run mongodb-community

Iremos a la terminal y ejecutamos este comando para levantar mongoDB en local

# https://robomongo.org/

Este programa nos conecta con Mongo y podemos ver mejor como es que esta funcionando

### npm run dev

Levanta un servidor en local para desarrollo el el puerto 4000, con nodemon

### npm start

Levanta un servidor en local para produccion, con node

# ENDPOINTS

A partir de aquio tendremos diversas rutas las cuales podran ser consumidas mediante POSTMAN o similar, o desde algun front

# / (HOME)

Ruta principal del proyecto

# /persona (GET)

hace una peticion get a todas las personas que contenga la base de datos


# /persona/:id (GET)

Nos trae la informacion de la persona cuyo id le pasemos

# /persona/nueva (POST)

Hacemos un POST, tenemos que pasarle 2 campos:
    - username
    - email
Ambos de tipo String

# /persona/modificar/:id (PUT)

Le pasamos el id de la persona que queramos modificar y hace un PUT

# /persona/borrar/:id  (DELETE)

Borramos la persona que indiquemos mediante el id que le pasemos